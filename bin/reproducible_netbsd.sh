#!/usr/bin/env bash

# Copyright 2014-2022 Holger Levsen <holger@layer-acht.org>
# Copyright 2023-2024 Jan-Benedict Glaw <jbglaw@lug-owl.de>
# Released under the GPLv2.


PAGE=netbsd/netbsd.html
TIMEOUT="30m"
DEBUG=true

. /srv/jenkins/bin/common-functions.sh
common_init "${@}"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

set -e
set -x

# Build for these architectures/ports.
declare -a machines	# Contains <machine>-<arch> (= <port>-<cpu>) pairs.
machines+=( sparc64-sparc64 )
#machines+=( amd64-x86_64 )

# How much Noise shall NetBSD's build process generate?
declare -a nb_noise
nb_noise=( -N 1 )

# Temporary debug function.
DG() {
	echo "LOGJJ ($(date)): $(pwd) - ${*}" >&2
}

cleanup_tmpdirs() {
	DG "In cleanup_tmpdirs, TMPDIR=${TMPDIR}, TMPBUILDDIR=${TMPBUILDDIR}"
	rm -r "${TMPDIR}"
	rm -r "${TMPBUILDDIR}"
}

create_results_dirs() {
	DG "In create_results_dirs"
	mkdir -p "${BASE}/netbsd/dbd"
}

# $1 - build ("b1", "b2", ...)
# $2 - machine ("x86_64-amd64")
# $3 - release dir
save_netbsd_results() {
	local run="${1}"; shift
	local full_machine="${1}"; shift
	local release_dir="${1}"; shift

	DG "save_netbsd_results: For build ${run} of machine ${full_machine}, copying ${release_dir} to $TMPDIR/${run}/${full_machine}"

	mkdir -p "${TMPDIR}/${run}/${full_machine}"
	(cd "${release_dir}" && tar cf - .) | ( cd "${TMPDIR}/${run}/${full_machine}" && tar xf -)
	find "${TMPDIR}/${run}/${full_machine}" \( -name MD5 -o -name SHA512 \) -exec rm {} \;
}

#
# main
#
TMPBUILDDIR=$(mktemp --tmpdir=/srv/workspace/chroots/   -d -t rbuild-netbsd-build-XXXXXXXX)   # used to build on tmpfs
TMPDIR=$(     mktemp --tmpdir=/srv/reproducible-results -d -t rbuild-netbsd-results-XXXXXXXX) # accessible in schroots, used to compare results
script_start_time=$(date -u +'%Y-%m-%d')
START=$(date +'%s')	# needed for later `calculate_build_duration` call.
trap cleanup_tmpdirs INT TERM EXIT

pushd "${TMPBUILDDIR}"
	DG "main: Checking our NetBSD src GIT tree"

	# Prepare sources.
	echo "============================================================================="
	echo "$(date -u) - Cloning the NetBSD git repository (which is synced with the NetBSD CVS repository)"
	echo "============================================================================="
	git clone --depth 1 https://github.com/NetBSD/src.git netbsd

	# Get current top commit infos.
	pushd netbsd
		DG "main: Fetching some GIT data items"
		netbsd_top_commit_log="$(git log -1)"
		netbsd_top_commit_rev="$(git describe --always)"
		netbsd_top_commit_epoch="$(git log -1 --format=%ct)"
		echo "This is NetBSD rev=${netbsd_top_commit_rev}."
		echo "This is NetBSD epoch=${netbsd_top_commit_epoch}."
		echo "Top commit:"
		git log -1
	popd

	# First round of builds. Run in a subshell to protect environment.
	(
		echo "============================================================================="
		echo "$(date -u) - Building NetBSD ${netbsd_top_commit_rev} - first build run."
		echo "============================================================================="
		export TZ="/usr/share/zoneinfo/Etc/GMT+12"
		for this_machine in "${machines[@]}"; do
			DG "main: Building NB #1 for ${this_machine}"

			this_subdir="b1-${this_machine}"
			this_mach="$(echo "${this_machine}" | cut -f 1 -d -)"
			this_arch="$(echo "${this_machine}" | cut -f 2 -d -)"

			# Create a pristine linked tree.
			cp -Rl netbsd "${this_subdir}"

			# Build and store results.
			pushd "${this_subdir}"
				release_dir=_release_
				full_release_dir="$(realpath "${release_dir}")"
				mkdir -p "${release_dir}"

				DG "main: Actually building NB #1 for ${this_machine}, results in ${full_release_dir}"
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" tools		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" release		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" iso-image		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" install-image	|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" live-image	|| true

				save_netbsd_results b1 "${this_machine}" "${full_release_dir}"
				echo "${this_machine} done, first time."
			popd

			# Clean up.
			DG "main: Cleaning up NB #1 for ${this_machine}"
			rm -rf "${this_subdir}"
		done
	)

	# Second round of builds. Also in a subshell to not taint our own environment.
	(
		echo "============================================================================="
		echo "$(date -u) - Building NetBSD ${netbsd_top_commit_rev} - second build run."
		echo "============================================================================="
		export TZ="/usr/share/zoneinfo/Etc/GMT-14"
		export LANG="fr_CH.UTF-8"
		export LC_ALL="fr_CH.UTF-8"
		export PATH="/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/i/capture/the/path"
		export CAPTURE_ENVIRONMENT="I capture the environment"
		umask 0002
		new_num_cpu=$(( ${NUM_CPU} - 1 ))
		for this_machine in "${machines[@]}"; do
			DG "main: Building NB #2 for ${this_machine}"

			this_subdir="b2-${this_machine}"
			this_mach="$(echo "${this_machine}" | cut -f 1 -d -)"
			this_arch="$(echo "${this_machine}" | cut -f 2 -d -)"

			# Create a pristine linked tree.
			cp -Rl netbsd "${this_subdir}"

			# Build and store results.
			pushd "${this_subdir}"
				release_dir=_release_
				full_release_dir="$(realpath "${release_dir}")"
				mkdir -p "${release_dir}"

				DG "main: Actually building NB #1 for ${this_machine}, results in ${full_release_dir}"
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" tools		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" release		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" iso-image		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" install-image	|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" live-image	|| true

				save_netbsd_results b2 "${this_machine}" "${full_release_dir}"
				echo "${this_machine} done, second time."
			popd

			# Clean up.
			DG "main: Cleaning up NB #2 for ${this_machine}"
			rm -rf "${this_subdir}"
		done
	)

	# Delete sources.
	DG "main: Cleaning initial NetBSD GIT checkout"
	rm -rf netbsd
popd	# from ${TMPBUILDDIR}


#
# Compare results and create a nice HTML page.
#

# Prepare a file list
declare -a file_list
create_results_dirs
file_list=( $( (cd "${TMPDIR}/b1" && find . -type f; cd "${TMPDIR}/b2" && find . -type f) | sort | uniq ) )

# Which diffoscope are we using?
diffoscope_version="$(schroot --directory /tmp -c "chroot:jenkins-reproducible-${DBDSUITE}-diffoscope" diffoscope -- --version 2>&1)"
echo "============================================================================="
echo "$(date -u) - Running ${diffoscope_version} on NetBSD build results..."
echo "============================================================================="


# Prepare HTML text containing information of all artifacts.
local filelist_output=$(mktemp --tmpdir="${TMPDIR}")
local last_machine=""
local curr_machine=""
local file_b1
local file_b2
local num_all_files=0
local num_good_files=0
(
	printf '<table>\n'
	for one_file in "${file_list[@]}"; do
		let num_all_files+=1

		# Do we need a new heading?
		curr_machine="$(echo "${one_file}" | cut -f 2 -d '/')"
		if [ "${curr_machine}" != "${last_machine}" ]; then
			printf '\t<tr>\n'
			printf '\t\t<th span="3" align="center">%s</th>\n' "${curr_machine}"
			printf '\t</tr>\n'
			last_machine="${curr_machine}"
		fi

		# Locate files.
		file_b1="${TMPDIR}/b1/${one_file}"
		file_b2="${TMPDIR}/b2/${one_file}"
		[ -r "${file_b1}" ] || file_b1=/dev/null
		[ -r "${file_b2}" ] || file_b2=/dev/null

		# Clean up stale (old/former) diffoscope result.
		rm -f "${BASE}/netbsd/dbd/${one_file}.html"

		# Compare b1 + b2 files and add a row to the table.
		if $(cmp "${file_b1}" "${file_b2}" > /dev/null 2>&1); then
			# File is reproducible.

			let num_good_files+=1

			printf '\t\t\t\t<tr>\n'
			printf '\t\t\t\t\t<td><img src="/userContent/static/weather-clear.png" alt="reproducible icon" /></td>\n'
			printf '\t\t\t\t\t<td>reproducible file:</td>\n'
			printf '\t\t\t\t\t<td>%s</td>\n' "${one_file}"
			printf '\t\t\t\t</tr>\n'
		else
			# File is not reproducible.

			call_diffoscope "." "${one_file}"
			mv -v "${TMPDIR}/${one_file}.html" "${BASE}/netbsd/dbd/${one_file}.html" || true
			printf '\t\t\t\t<tr>\n'
			printf '\t\t\t\t\t<td><img src="/userContent/static/weather-showers-scattered.png" alt="unreproducible icon" /></td>\n'
			printf '\t\t\t\t\t<td><a href="dbd/%s.html">issues</a> in file:</td>\n' "${one_file}"
			printf '\t\t\t\t\t<td>%s</td>\n' "${one_file}"
			printf '\t\t\t\t</tr>\n'
		fi
	done
	printf '</table>\n'
) > "${filelist_output}"

# Calculate percentage of reproducible files.
good_percent="$(echo "scale=1; (${num_good_files}*100/$num_all_files)" | bc)"
if [ "${good_percent}" = 100.0 ]; then
	magic_sign="!"
else
	magic_sign="?"
fi



#
# Finally create the web page.
#
pushd "${TMPDIR}"
	mkdir -p netbsd
	cat > "${PAGE}" <<- EOF
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<title>Reproducible NetBSD ${magic_sign}</title>
		<link rel='stylesheet' href='global.css' type='text/css' media='all' />
	</head>
	<body>
		<div id="logo">
			<img src="NetBSD-smaller.png" />
			<h1>Reproducible NetBSD ${magic_sign}</h1>
		</div>
		<div class="content">
			<div class="page-content">
EOF
	write_page_intro NetBSD
	write_page "				<p>${num_good_files} (${good_percent}%) out of ${num_all_files} built NetBSD files were reproducible in our test setup${magic_sign}</p>"
	write_page "				<p>These tests were last run on ${script_start_time} for rev ${netbsd_top_commit_rev} with -P (as of @${netbsd_top_commit_epoch}) and were compared using ${diffoscope_version}.</p>"
	write_variation_table NetBSD
	cat "${filelist_output}" >> "${PAGE}"
	write_page "				<p><pre>"
	echo -n "${netbsd_top_commit_log}" >> "${PAGE}"
	write_page "				</pre></p>"
	write_page "			</div>"
	write_page "		</div>"
	write_page_footer NetBSD
	publish_page
	rm -f "${filelist_output}"
popd

#
# The end.
#
calculate_build_duration	# Sets ${DURATION}.
print_out_duration		# Uses ${DURATION} calculated above.
irc_message reproducible-changes "${REPRODUCIBLE_URL}/netbsd/ has been updated. (${good_percent}% reproducible)"

# Remove everything, we don't need it anymore...
cleanup_tmpdirs
trap - INT TERM EXIT
