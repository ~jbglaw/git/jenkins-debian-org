#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright © 2015-2024 Holger Levsen <holger@layer-acht.org>
#           © 2018      Mattia Rizzolo <mattia@mapreri.org>
# based on ~jenkins.d.n:~mattia/status.sh by Mattia Rizzolo <mattia@mapreri.org>
# Licensed under GPL-2
#
# Depends: python3

from string import Template
from sqlalchemy import select, func, cast, Integer, and_, bindparam

from rblib import query_db, db_table
from rblib.confparse import log
from rblib.models import Package
from rblib.html import tab, create_main_navigation, write_html_page, convert_into_status_html
from reproducible_html_indexes import build_leading_text_section
from rblib.const import (
    DISTRO_BASE, DISTRO_URL, DISTRO_URI,
    ARCHS, SUITES,
    defaultsuite,
)

# sqlalchemy table definitions needed for queries
results = db_table('results')
sources = db_table('sources')
schedule = db_table('schedule')
stats_build = db_table('stats_build')


def gen_suitearch_oldies_table(suite, arch):
    html = ''
    query = select([
        sources.c.suite,
        sources.c.architecture,
        sources.c.name,
        results.c.status,
        results.c.build_date
    ]).select_from(
        results.join(sources)
    ).where(
        and_(
            sources.c.suite == bindparam('suite'),
            sources.c.architecture == bindparam('arch'),
            results.c.status != 'blacklisted'
        )
    ).order_by(
        results.c.build_date
    ).limit(15)
    text = Template('Oldest results for $suite/$arch:')
    rows = query_db(query.params({'arch': arch, 'suite': suite}))
    html += build_leading_text_section({'text': text}, rows, suite, arch)
    html += '<p><table class="scheduled">\n' + tab
    html += '<tr><th class="center">#</th><th class="center">suite</th><th class="center">arch</th>'
    html += '<th class="center">source package</th><th class="center">status</th><th class="center">build date</th></tr>\n'
    for row in rows:
        # 0: suite, 1: arch, 2: pkg name 3: status 4: build date
        pkg = row[2]
        html += tab + '<tr><td>&nbsp;</td><td>' + row[0] + '</td>'
        html += '<td>' + row[1] + '</td><td><code>'
        html += Package(pkg).html_link(row[0], row[1])
        html += '</code></td><td>'+convert_into_status_html(str(row[3]))+'</td><td>' + str(row[4]) + '</td></tr>\n'
    html += '</table></p>\n'
    return html

def generate_oldies(arch):
    log.info('Building the oldies page for ' + arch + '...')
    title = 'Oldest results for ' + arch
    html = ''
    # reorder suites, to list experimental first
    reordered_suites = SUITES
    reordered_suites.insert(0, reordered_suites.pop())
    for suite in reordered_suites:
        html += gen_suitearch_oldies_table(suite, arch)
    destfile = DISTRO_BASE + '/index_' + arch + '_oldies.html'
    desturl = DISTRO_URL + '/index_' + arch + '_oldies.html'
    left_nav_html = create_main_navigation(arch=arch)
    write_html_page(title=title, body=html, destfile=destfile, style_note=True,
                    refresh_every=60, left_nav_html=left_nav_html)
    log.info("Page generated at " + desturl)

if __name__ == '__main__':
    for arch in ARCHS:
        generate_oldies(arch)
