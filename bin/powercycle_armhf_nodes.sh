#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021-2022 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

CONSOLESERVER=hbi1.dyn.aikidev.net

if [ -z "$1" ] ; then
	echo $0 needs one or more params:
	./nodes/list_nodes | grep armhf | cut -d '-' -f1
fi

# validate input and modify if needed
for i in "$@" ; do
	case $i in
		bpi0|hb0)	NODE=${i}a	;;
		virt*)		echo "Sorry, I do not yet know how to handle $i, skipping."
				echo "But the steps are documented in README.infrastructure."
				echo "Additionally one can powercycle mst0X using $0."
				continue
				;;
		*-*)		NODE=$(echo $i | cut -d '-' -f1) ;;
		*)		NODE=$i		;;
	esac
	echo "Power cycling $NODE ..."
	ssh $CONSOLESERVER "cd /srv/rb/remote-power ; ./remote-power $NODE ccl"
done

