#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright 2009-2021 Holger Levsen (holger@layer-acht.org)
#
# based on similar code taken from piuparts-reports.py written by me

import os
import sys
from rpy2 import robjects
from rpy2.robjects.packages import importr

def main():
    if len(sys.argv) != 8:
        print("we need exactly seven params: csv-file-in, png-out-file, color, mainlabel, ylabl, width, height")
        return
    filein = sys.argv[1]
    fileout = sys.argv[2]
    colors = sys.argv[3]
    columns = str(int(colors)+1)
    mainlabel = sys.argv[4]
    ylabel = sys.argv[5]
    width = int(sys.argv[6])
    height = int(sys.argv[7])
    countsfile = os.path.join(filein)
    pngfile = os.path.join(fileout)
    grdevices = importr('grDevices')
    grdevices.png(file=pngfile, width=width, height=height, pointsize=10, res=100, antialias="none")
    r = robjects.r
    r('t <- (read.table("'+countsfile+'",sep=",",header=1,row.names=1))')
    r('cname <- c("date",rep(colnames(t)))')
    # thanks to http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines for those nice colors
    if int(colors) < 6:
        r('palette(c("#73d216", "#f57900", "#cc0000", "#2e3436", "#888a85"))')
    elif int(colors) == 6:
        r('palette(c("#73d216", "#f57900", "#cc0000", "#2e3436", "#ffdd00", "#aaaaaa"))')
    elif int(colors) == 99:
        # each row represents one state: reproducible, FTBR, FTBFS, other
        # each column represents one suite:
        #             stretch    buster     bullseye   bookworm   trixie     unstable   experimental
        colors='28'   # ADJUST THIS NUMBER TOO: 28 = 8 releases * 4 states
        r('palette(c("#4e9a06", "#57a231", "#73d216", "#8ae234", "#9af244", "#acff65", "#afff85", \
                     "#ce5c00", "#d56900", "#f57900", "#fcaf3e", "#fc9a27", "#fdba37", "#facea2", \
                     "#930000", "#a40000", "#cc0000", "#e21100", "#fc2a00", "#fd1a1a", "#ff0a2a", \
                     "#222222", "#333333", "#444444", "#555555", "#666666", "#777777", "#888888" ))')
    elif int(colors) == 101:
        r('palette(c("#4e9a06", "#000000"))')   # stretch
    elif int(colors) == 102:
        r('palette(c("#57a231", "#000000"))')   # buster
    elif int(colors) == 103:
        r('palette(c("#73d216", "#000000"))')   # bullseye
    elif int(colors) == 104:
        r('palette(c("#8ae234", "#000000"))')   # bookworm
    elif int(colors) == 105:
        r('palette(c("#9af244", "#000000"))')   # trixie
    elif int(colors) == 200:
        r('palette(c("#acff65", "#000000"))')   # unstable
    elif int(colors) == 300:
        r('palette(c("#afff95", "#000000"))')   # experimental
    else:
	# used for the user-tagged bugs graphs
        r('palette(c("#fce94f", "#c4a000", "#eeeeec", "#babdb6", \
                     "#fcaf3e", "#ce5c00", "#ad7fa8", "#5c3566", \
                     "#e9b96e", "#8f5902", "#8ae234", "#4e9a06", \
                     "#729fcf", "#204a87", "#ef2929", "#a40000", \
                     "#888a85", "#2e3436", "#75507b", "#cc0000", \
                     "#ce5c00", "#73d216", "#edd400", "#f57900", \
                     "#c17d11", "#3465a4", "#666666", "#aaaaaa", \
                     "#aa00aa", "#ff55ff", "#123456", "#7890ab" ))')
    # "revert the hack" - it's still a hack though
    if int(colors) >= 100:
        colors='1'
    r('v <- t[0:nrow(t),0:'+colors+']')
    # make graph since day 1
    r('barplot(t(v),col = 1:'+columns+', main="'+mainlabel+'", xlab="", ylab="'+ylabel+'", space=0, border=NA, names.arg=rownames(t))')
    if int(colors) < 10:
        r('legend(x="bottom",legend=colnames(t), ncol=2,fill=1:'+columns+',xjust=0.5,yjust=0,bty="n")')
    elif int(colors) == 16:
        r('legend(x="bottom",legend=colnames(t), ncol=4,fill=1:'+columns+',xjust=0.5,yjust=0,bty="n")')
    else:
        r('legend(x="bottom",legend=colnames(t), ncol=7,fill=1:'+columns+',xjust=0.5,yjust=0,bty="n")')
    grdevices.dev_off()

if __name__ == "__main__":
    main()

# vi:set et ts=4 sw=4 :
